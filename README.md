# yapdfkitwrapper
Yet Another PDFKit Wrapper.

A node module to generate a PDF from plain javascript object.
It uses [PDFKit](http://pdfkit.org/).
It is very feature incomplete, till now I only implemented the features I
needed myself. But still it might be helpful to someone so I wanted to
publish it.


## Features
- Create PDF document with some text
- Define a background image that will be placed on all the pages
- Define a header and footer with page numbers
- Simple API

## Installing
```
npm install yapdfkitwrapper --save
```

## Usage
Load as in the following example. Then use print() function as described
in the next section.

```javascript
var yapdfkitwrapper = require('yapdfkitwrapper');
```

### Print
Printing a document is easy.
The options listed below are shown with their defaults. You only
need to set them if the default should be changed.

You can use the test/test.js file to generate a PDF quickly.

```javascript
var doc = {
  options: {
    subject: '', // used as file property
    author: '', // used as file property
    size: 'a4',
    margins: { // in PDF points
      top: 141,
      bottom: 128,
      left: 72,
      right: 57
    },
    font: 'Helvetica',
    fontSize: 10,
    fillColor: 'black',
    align: 'left',
    background: fs.readFileSync('./logo.png') // or just background: './logo.png', default: no background
  },
  content: [
    {
      font: 'Helvetica',
      fontSize: '10',
      fillColor: 'black',
      align: 'left',
      text: 'Some text using the default layout'
    },
    {
      font: 'Courier',
      fontSize: '16',
      fillColor: 'white',
      align: 'right',
      position: { // in PDF points
        x: 250,
        y: 250
      },
      text: 'Some text with changed layout and postion'
    }
  ],
  header: {
    content: [
      {
        font: 'Helvetica',
        fontSize: '8',
        fillColor: 'grey',
        position: {
          x: 72,
          y: 100
        },
        align: 'left',
        text: 'Header with default layout and position'
      }
    ]
  },
  footer: {
    content: [
      {
        font: 'Helvetica',
        fontSize: '8',
        fillColor: 'grey',
        position: {
          x: 72,
          y: 'height - 100' // evaluated with current page height, can also be fixed no.
        },
        align: 'left',
        text: 'Footer with default layout and position'
      },
      {
        align: 'right',
        text: 'Page {page} of {pages}' // is replaced by current page and total page count
      }
    ]
  }
};

var stream = fs.createWriteStream('./test.pdf');
yapdfkitwrapper.print(doc, stream, function(err, result) {
  console.log(result); // e.g. prints {pages: 2}
}); // file can be found as ./test.pdf
```

[Here is the result](https://bitbucket.org/simonbernard/yapdfkitwrapper/raw/74bb47032f582ebcf75a26f3caae3c3ff5b3cb50/test/test.pdf)

## Tests
No automated tests available but tested!

## Unsupported
A lot of PDFKit features are not yet supported, contributions welcome!
