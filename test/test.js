
var fs = require('fs');
var yapdfkitwrapper = require('../yapdfkitwrapper');

var doc = {
	options: {
		subject: '', // used as file property
		author: '', // used as file property
		size: 'a4',
		margins: {// in PDF points
			top: 141,
			bottom: 128,
			left: 72,
			right: 57
		},
		font: 'Helvetica',
		fontSize: 10,
		fillColor: 'black',
		align: 'left',
		background: fs.readFileSync('./old_paper.jpg') // or just background: './old_paper.jpg', default: no background
	},
	content: [
		{
			font: 'Helvetica',
			fontSize: '10',
			fillColor: 'black',
			align: 'left',
			text: 'Some text using the default layout'
		},
		{
			font: 'Courier',
			fontSize: '16',
			fillColor: 'white',
			align: 'right',
			position: {// in PDF points
				x: 250,
				y: 250
			},
			text: 'Some text with changed layout and postion'
		}
	],
	header: {
		content: [
			{
				font: 'Helvetica',
				fontSize: '8',
				fillColor: 'grey',
				position: {
					x: 72,
					y: 100
				},
				align: 'left',
				text: 'Header with default layout and position'
			}
		]
	},
	footer: {
		content: [
			{
				font: 'Helvetica',
				fontSize: '8',
				fillColor: 'grey',
				position: {
					x: 72,
					y: 'height - 100' // evaluated with current page height, can also be fixed no.
				},
				align: 'left',
				text: 'Footer with default layout and position'
			},
			{
				align: 'right',
				text: 'Page {page} of {pages}' // is replaced by current page and total page count
			}
		]
	}
};

var stream = fs.createWriteStream('./test.pdf');
yapdfkitwrapper.print(doc, stream); // file can be found as ./test.pdf
