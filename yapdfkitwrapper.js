'use strict';

var format = require('string-template');
var _ = require('lodash');
var PDFDocument = require('pdfkit');

var YAPdfKitWrapper = function () {
};

YAPdfKitWrapper.prototype.print = function (doc, stream, callback) {

	callback = callback || function () {
	};

	// pdf meta information
	var info = {
		Title: doc.options.subject || '',
		Subject: doc.options.subject || '',
		Author: doc.options.author || '',
		Producer: 'yapdfkitwrapper',
		Creator: 'yapdfkitwrapper'
	};

	var options = _.merge({
		size: 'a4',
		bufferPages: true,
		info: info,
		margins: {
			top: 141,
			bottom: 128,
			left: 72,
			right: 57
		},
		font: 'Helvetica',
		fontSize: 10,
		fillColor: 'black',
		align: 'left'
	}, doc.options);
	var pdf = new PDFDocument(options);

	pdf.pipe(stream);
	if (options.background) {
		pdf.image(doc.options.background, 0, 0, {width: pdf.page.width, height: pdf.page.height});
		pdf.on('pageAdded', function () {
			pdf.image(doc.options.background, 0, 0, {width: pdf.page.width, height: pdf.page.height});
		});
	}

	_(doc.content).each(function (c) {
		pdf.font(c.font || options.font)
			.fontSize(c.fontSize || options.fontSize)
			.fillColor(c.fillColor || options.fillColor);
		if (c.position)
			pdf.text(c.text, c.position.x, c.position.y, {align: c.align || options.align});
		else
			pdf.text(c.text, {align: c.align || options.align});
	});

	var pageRange = pdf.bufferedPageRange();
	for (var i = pageRange.start; i < pageRange.count; i++) {
		pdf.switchToPage(i);

		// header
		pdf.page.margins.top = 0;
		if (doc.header) {
			_(doc.header.content).each(function (c) {
				pdf.font(c.font || options.font)
					.fontSize(c.fontSize || 8)
					.fillColor(c.fillColor || 'grey');
				var text = format(c.text, {
					page: i + 1,
					pages: pageRange.count
				});
				if (c.position)
					pdf.text(text, c.position.x, c.position.y, {align: c.align || options.align});
				else
					pdf.text(text, 72, 100, {align: c.align || options.align});
			});
			pdf.page.margins.top = options.margins.top;
		}

		// footer and page numbers
		if (doc.footer) {
			pdf.page.margins.bottom = 0;
			_(doc.footer.content).each(function (c) {
				pdf.font(c.font || options.font)
					.fontSize(c.fontSize || 8)
					.fillColor(c.fillColor || 'grey');
				var text = format(c.text, {
					page: i + 1,
					pages: pageRange.count
				});
				if (c.position)
					pdf.text(text, c.position.x, eval('pdf.page.' + c.position.y), {align: c.align || options.align});
				else
					pdf.text(text, 72, pdf.page.height - 100, {align: c.align || options.align});
			});
			pdf.page.margins.bottom = options.margins.bottom;
		}
	}

	pdf.end();

	// pdf successfully generated
	return callback(null, {pages: pageRange.count});
};

module.exports = new YAPdfKitWrapper();
